package ru.t1.dkozoriz.tm.api.service;

import ru.t1.dkozoriz.tm.model.Task;

import java.util.List;

public interface ITaskService extends IAbstractService<Task> {

    List<Task> findAllByProjectId(String ProjectId);

}