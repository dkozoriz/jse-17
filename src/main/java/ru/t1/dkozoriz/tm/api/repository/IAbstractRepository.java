package ru.t1.dkozoriz.tm.api.repository;

import ru.t1.dkozoriz.tm.api.model.IWBS;

import java.util.Comparator;
import java.util.List;

public interface IAbstractRepository<T> {

    List<T> findAll();

    List<T> findAll(Comparator<? super IWBS> comparator);

    T add(T project);

    void clear();

    T findOneById(String id);

    T findOneByIndex(Integer index);

    int getSize();

    void remove(T project);

    T removeById(String id);

    T removeByIndex(Integer index);

}
