package ru.t1.dkozoriz.tm.repository;

import ru.t1.dkozoriz.tm.api.repository.ICommandRepository;
import ru.t1.dkozoriz.tm.command.AbstractCommand;


import java.util.*;

public final class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> mapByArgument = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> mapByName = new LinkedHashMap<>();

    public void add(final AbstractCommand command) {
        if (command == null) return;
        final String name = command.getName();
        final String argument = command.getArgument();
        if (name != null) mapByName.put(name, command);
        if (argument != null) mapByArgument.put(argument, command);
    }

    public AbstractCommand getCommandByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return mapByName.get(name);
    }

    public AbstractCommand getCommandByArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return null;
        return mapByArgument.get(argument);
    }

    public Collection<AbstractCommand> getCommands() {
        return mapByName.values();
    }

}