package ru.t1.dkozoriz.tm.command.system;

import ru.t1.dkozoriz.tm.command.AbstractCommand;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    private static final String NAME = "help";

    private static final String ARGUMENT = "-h";

    private static final String DESCRIPTION = "show command list.";

    public String getName() {
        return NAME;
    }

    public String getArgument() {
        return ARGUMENT;
    }

    public String getDescription() {
        return DESCRIPTION;
    }

    public void execute() {
        for (final AbstractCommand command : serviceLocator.getCommandService().getCommands()) {
            System.out.println(command);
        }
    }

}
