package ru.t1.dkozoriz.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    private static final String NAME = "about";

    private static final String ARGUMENT = "-a";

    private static final String DESCRIPTION = "show developer info.";

    public String getName() {
        return NAME;
    }

    public String getArgument() {
        return ARGUMENT;
    }

    public String getDescription() {
        return DESCRIPTION;
    }

    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Daria Kozoriz");
        System.out.println("E-mail: dkozoriz@t1-consulting.ru");
    }

}
