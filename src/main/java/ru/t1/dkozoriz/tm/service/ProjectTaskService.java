package ru.t1.dkozoriz.tm.service;

import ru.t1.dkozoriz.tm.api.repository.IProjectRepository;
import ru.t1.dkozoriz.tm.api.repository.ITaskRepository;
import ru.t1.dkozoriz.tm.api.service.IProjectTaskService;
import ru.t1.dkozoriz.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkozoriz.tm.exception.entity.TaskNotFoundException;
import ru.t1.dkozoriz.tm.exception.field.ProjectIdEmptyException;
import ru.t1.dkozoriz.tm.exception.field.TaskIdEmptyException;
import ru.t1.dkozoriz.tm.model.Project;
import ru.t1.dkozoriz.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public Task bindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        final Project project = projectRepository.findOneById(projectId);
        if (project == null) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        return task;
    }

    public void removeProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        final Project project = projectRepository.findOneById(projectId);
        if (project == null) throw new ProjectNotFoundException();
        final List<Task> tasks = taskRepository.findAllByProjectId(projectId);
        for (final Task task : tasks) taskRepository.removeById(task.getId());
        projectRepository.removeById(projectId);
    }

    public Task unbindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        final Project project = projectRepository.findOneById(projectId);
        if (project == null) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        return task;
    }

}